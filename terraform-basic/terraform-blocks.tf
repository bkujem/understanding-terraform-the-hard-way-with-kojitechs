
## TERRAFORM BLOCK-this block is meant to set a constraint on terraform version

terraform {
    required_version = ">=1.1.0"
  required_providers {
    aws = {
      source  = "hashicorp/aws"
      version = "~> 4.0"
    }
  }
}

## PROVIDER BLOCK: (Authentication & Authorization)
## Best method which does not expose your credentials
provider "aws" {
    region = "us-east-1"
  profile = "default"
}

## RESOURCE BLOCK : (create resources/bring into existence)

resource "aws_vpc" "main" {
  cidr_block       = "10.0.0.0/16"
  instance_tenancy = "default"

  tags = {
    Name = "main"
  }
}


# How to reference a variable-by using the command var.variable name eg. var.ami_id

## Data Source: (if a resource already exist, use data source to pull down available resources in aws)cd

# Creating EC2
 ##       Local name     resource name
 ## NB: You can't change the local name, but you cam change the resource name

#resource "aws_instance" "ec2_instance" {
# ami           = var.ami_id #var.ami_id references the variable block created above
# instance_type = "t2.micro"

#tags = {
 # Name = "ec2_instance"

## Creating ec2
      #   Local name   Resource Name
resource "aws_instance" "foo" {
  ami   = var.ami_id
  instance_type = "t2.micro"

  tags ={
    Name = "ec2_instance"
  }

}

